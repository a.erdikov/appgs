<?php
/**
 * Created by PhpStorm.
 * User: qposer
 * Date: 15.08.18
 * Time: 16:39
 */

namespace app\bootstrap;


use app\models\rules\BaseRule;
use app\models\rules\CountryRule;
use app\services\BannerService;
use yii\base\BootstrapInterface;

class SetUp implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $contaner = \Yii::$container;

        $contaner->setSingleton(BannerService::class, function() use ($app) {
            return new BannerService(
                new CountryRule(new BaseRule())
            );
        });
    }
}