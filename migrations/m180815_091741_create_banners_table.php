<?php

use yii\db\Migration;

/**
 * Handles the creation of table `banners`.
 */
class m180815_091741_create_banners_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%banners}}', [
            'id' => $this->primaryKey(),
            'content' => $this->string(255),
        ]);

        $this->execute('INSERT INTO `appg_test`.`banners` (`id`, `content`) VALUES (1, \'banner1\')');
        $this->execute('INSERT INTO `appg_test`.`banners` (`id`, `content`) VALUES (2, \'banner2\')');
        $this->execute('INSERT INTO `appg_test`.`banners` (`id`, `content`) VALUES (3, \'banner3\')');
        $this->execute('INSERT INTO `appg_test`.`banners` (`id`, `content`) VALUES (4, \'banner4\')');
        $this->execute('INSERT INTO `appg_test`.`banners` (`id`, `content`) VALUES (5, \'banner5\')');
        $this->execute('INSERT INTO `appg_test`.`banners` (`id`, `content`) VALUES (6, \'banner6\')');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('banners');
    }
}
