<?php
/**
 * Created by PhpStorm.
 * User: qposer
 * Date: 15.08.18
 * Time: 16:11
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class Banner
 * @package app\models
 */
class Banner extends ActiveRecord
{

    public function __toString()
    {
        return (string)$this->content;
    }

    public static function tableName()
    {
        return '{{%banners}}';
    }

}