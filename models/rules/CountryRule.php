<?php
/**
 * Created by PhpStorm.
 * User: qposer
 * Date: 15.08.18
 * Time: 17:11
 */

namespace app\models\rules;


use app\models\Banner;
use app\models\rules\country\RuRule;
use app\models\rules\country\UsaRule;
use yii\web\Request;

class CountryRule implements RuleInterface
{
    /**
     * @var RuleInterface
     */
    private $next;

    public function __construct(RuleInterface $rule)
    {
        $this->next = $rule;
    }

    /**
     * @param Request $request
     * @return Banner
     */
    public function getBanner(Request $request): Banner
    {
        $details = json_decode(file_get_contents('http://ipinfo.io/' . $request->getUserIP()));

        if (isset($details->country)) {
            switch ($details->country) {
                case 'RU':
                        return (new RuRule($this->next))->getBanner($request);
                    break;
                case 'USA':
                        return (new UsaRule($this->next))->getBanner($request);
                    break;
            }
        }

        return $this->next->getBanner($request);
    }
}