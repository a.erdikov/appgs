<?php
/**
 * Created by PhpStorm.
 * User: qposer
 * Date: 15.08.18
 * Time: 16:47
 */

namespace app\models\rules;


use app\models\Banner;
use yii\web\Request;

class BaseRule implements RuleInterface
{
    public function __construct(RuleInterface $rule = null)
    {
    }

    public function getBanner(Request $request): Banner
    {
        return Banner::findOne(['id' => 6]);
    }
}