<?php
/**
 * Created by PhpStorm.
 * User: qposer
 * Date: 15.08.18
 * Time: 16:49
 */

namespace app\models\rules\country;


use app\models\Banner;
use app\models\rules\RuleInterface;
use yii\web\Request;

class RuRule implements RuleInterface
{
    /**
     * @var RuleInterface
     */
    private $next;

    public function __construct(RuleInterface $rule)
    {
        $this->next = $rule;
    }

    /**
     * @param Request $request
     * @return Banner
     */
    public function getBanner(Request $request): Banner
    {
        $param = $request->get('test');
        if ($param && $param == 1) {
            return Banner::findOne(['id' => 3]);
        }

        if (rand(0,99) < 70) {
            return Banner::findOne(['id' => 4]);
        }

        return Banner::findOne(['id' => 5]);
    }
}