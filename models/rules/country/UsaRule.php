<?php
/**
 * Created by PhpStorm.
 * User: qposer
 * Date: 15.08.18
 * Time: 17:14
 */

namespace app\models\rules\country;


use app\models\Banner;
use app\models\rules\RuleInterface;
use yii\web\Request;

class UsaRule implements RuleInterface
{
    /**
     * @var RuleInterface
     */
    private $next;

    public function __construct(RuleInterface $rule)
    {
        $this->next = $rule;
    }

    /**
     * @param Request $request
     * @return Banner
     */
    public function getBanner(Request $request): Banner
    {
        if (rand(0,99) < 15) {
            return Banner::findOne(['id' => 1]);
        }

        return Banner::findOne(['id' => 2]);
    }
}