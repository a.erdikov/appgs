<?php
namespace app\models\rules;

use app\models\Banner;
use yii\web\Request;

interface RuleInterface
{
    public function __construct(RuleInterface $rule);

    /**
     * @param Request $request
     * @return Banner
     */
    public function getBanner(Request $request): Banner;

}