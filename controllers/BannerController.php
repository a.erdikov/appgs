<?php
/**
 * Created by PhpStorm.
 * User: qposer
 * Date: 15.08.18
 * Time: 16:07
 */

namespace app\controllers;


use app\services\BannerService;
use Yii;
use yii\web\Controller;

class BannerController extends Controller
{

    /**
     * @var BannerService
     */
    private $bannerService;

    public function __construct($id, $module, BannerService $bannerService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->bannerService = $bannerService;
    }

    public function actionIndex()
    {
        return $this->bannerService->getBannerByRequest(Yii::$app->request);
    }

}