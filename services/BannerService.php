<?php
namespace app\services;

use app\models\Banner;
use app\models\rules\RuleInterface;
use yii\web\Request;

class BannerService
{
    /**
     * @var RuleInterface
     */
    private $rule;

    public function __construct(RuleInterface $rule)
    {
        $this->rule = $rule;
    }

    public function getBannerByRequest(Request $request): Banner
    {
        return $this->rule->getBanner($request);
    }

}